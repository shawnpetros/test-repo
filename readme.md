# test repo

### testing out some git craziness

- repo created some commits were added
- branches created including a feature branch and a staging/releaase branch
- some changes were made to master for a bugfix
- bugfix was pulled into staging via cherry-pick
- master was merged into staging (created a merge commit)
- branches are compared

cherry pick direct to staging and re-base with master

- you can rebase off another branch but then that branch must be force pushed, can only happen in a non-protected branch and MRed, which defeats the purpose of rebasing.

chery pick to support fix into staging with MR

- seems to function with little to no inconsistencies

staging on next version - roll back to align with prod to allow prod fix

- fix is done in lower env, but feature branch is cut off of master and hotfix is cherry picked, then put into staging
